### To run the example app ###

This just loads the plugins Main.qml into a Page component and should function normally. This example only runs the autodiscover
plugin and populates the input fields from the found settings. Actually storing the account credentials still needs to be done.

```
#!bash

git clone https://bitbucket.org/dekkoproject/email-oa-plugin.git
cd email-oa-plugin
mkdir __build
cd __build
cmake ..
make run
```
You may need to install the libconnectivity-qt1-dev package if you don't already have it installed.

To run the unit tests it's the same as above apart from the make command should be 

```
#!bash

make check
```

### Still TODO ###
* debian packaging
* click packaging... do we ship this with dekko initially
* agree with others on default template settings available to all apps.