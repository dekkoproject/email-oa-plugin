/* Copyright (C) 2015 Dan Chapman <dpniel@ubuntu.com>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QtQml>
#include <QtQml/QQmlContext>
#include "servicechecker_plugin.h"
#include "servicevalidator.h"
#include "mailservicetask.h"
#include "saslmechanisms.h"


void ServiceCheckerPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("MailServiceValidator"));
    // @uri MailServiceChecker
    qmlRegisterType<ServiceValidator>(uri, 1, 0, "ServiceValidator");
    qmlRegisterSingletonType<SaslMechanisms>(uri, 1, 0, "SaslMechanisms", SaslMechanisms::factory);
    qRegisterMetaType<MailServiceTask::State>();
}

void ServiceCheckerPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    QQmlExtensionPlugin::initializeEngine(engine, uri);
}

